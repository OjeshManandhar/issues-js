function submitNewIssue(e, closeModal) {
  const form = e.target;

  const assignee = getAssigneeIdFromDatalist(
    form.issue__assignee.value,
    '#issue__suggestions'
  );

  const issue = new Issue(
    form.issue__title.value,
    form.issue__description.value,
    assignee
  );

  issue.save();

  closeModal();
}

function submitNewAssignee(e, closeModal) {
  const form = e.target;

  AssigneeList.add(form.assignee__name.value, form.assignee__description.value);

  closeModal();
}

document.addEventListener('DOMContentLoaded', () => {
  const issueModal = new Modal('#modal__issues', submitNewIssue);
  const assigneeModal = new Modal('#modal__assignees', submitNewAssignee);

  const addIssuesBtn = document.querySelector('.issues .btn.issues__add');
  const addAssigneesBtn = document.querySelector(
    '.assignees .btn.assignees__add'
  );

  addIssuesBtn.addEventListener('click', () =>
    issueModal.show(loadAssigneeNameInDatalist)
  );
  addAssigneesBtn.addEventListener('click', () => assigneeModal.show());

  AssigneeList.load();
  Issue.load();
  loadAssigneeNameInDatalist();
});
