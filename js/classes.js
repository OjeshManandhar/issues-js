// CSS class names
const dNone = 'd-none';

const ISSUE_STATUS = Object.freeze({
  OPEN: 'Open',
  ASSIGNED: 'Assigned',
  CLOSED: 'Closed'
});

const issueList = [
  {
    id: 'issue-0',
    title: 'Dummy',
    description: 'Just an issue',
    status: ISSUE_STATUS.ASSIGNED,
    assignee: 'assignee-1',
    created_at: new Date()
  }
];

class Modal {
  #elem = null;

  constructor(elemSelector, submitFunc) {
    this.#elem = document.querySelector(elemSelector);

    this.#elem
      .querySelector('.btn.modal__close')
      .addEventListener('click', () => this.close());

    this.#elem.querySelector('.modal__form').addEventListener('submit', e => {
      e.preventDefault();

      submitFunc(e, () => this.close());
    });
  }

  show(fn) {
    const modal = document.querySelector('.modal');

    if (fn && typeof fn === 'function') fn();

    // Show modal
    modal.classList.remove(dNone);

    // Hide all its child modal__content
    const modalContent = modal.querySelectorAll('.modal__content');
    modalContent.forEach(el => el.classList.add(dNone));

    // Show this modal__content
    this.#elem.classList.remove(dNone);
  }

  close() {
    const modal = document.querySelector('.modal');

    // Hide modal
    modal.classList.add(dNone);

    // Hide all its child modal__content i.e. also this modal
    const modalContent = modal.querySelectorAll('.modal__content');
    modalContent.forEach(elem => elem.classList.add(dNone));

    // Clear form
    this.#elem.querySelector('.modal__form').reset();
  }
}

class _AssigneeList {
  #newId = 1;
  #assigneeList = [
    {
      id: `assignee-${this.#newId++}`,
      name: 'Ojesh Manandhar',
      description: 'Backend Intern'
    }
  ];

  #createUiBlock(assignee) {
    const assignleListUI = document.querySelector(
      '.assignees .assignees__list'
    );

    const block = document.createElement('li');
    block.id = assignee.id;
    block.classList.add('assignees__block');
    assignleListUI.appendChild(block);

    const name = document.createElement('div');
    name.innerText = assignee.name;
    name.classList.add('assignees__name');
    block.appendChild(name);

    const deleteAssignee = document.createElement('div');
    deleteAssignee.innerText = 'X';
    deleteAssignee.classList.add('btn', 'assignees__delete');
    block.appendChild(deleteAssignee);
    deleteAssignee.onclick = () => this.#remove(assignee.id, block);

    const description = document.createElement('p');
    description.innerText = assignee.description;
    description.classList.add('assignees__description');
    block.appendChild(description);

    return block;
  }

  #remove(assigneeId, block) {
    this.#assigneeList = this.#assigneeList.filter(a => a.id !== assigneeId);

    loadAssigneeNameInDatalist();

    block.remove();
  }

  add(name, description) {
    const newAssignee = {
      id: `assignee-${this.#newId++}`,
      name: name.trim(),
      description: description.trim()
    };

    this.#assigneeList.push(newAssignee);

    loadAssigneeNameInDatalist();

    this.#createUiBlock(newAssignee);
  }

  list() {
    return this.#assigneeList.map(item => ({ id: item.id, name: item.name }));
  }

  find(id) {
    return this.#assigneeList.find(assignee => assignee.id === id);
  }

  load() {
    this.#assigneeList.forEach(assignee => {
      this.#createUiBlock(assignee);
    });
  }
}

class Issue {
  static #newId = 1;

  #id;
  #title;
  #description;
  #status;
  #assignee;
  #created_at;

  constructor(title, description, assignee) {
    this.#id = 'issue-' + Issue.#newId++;
    this.#title = title.trim();
    this.#description = description.trim();
    if (assignee) {
      this.#status = ISSUE_STATUS.ASSIGNED;
      this.#assignee = assignee;
    } else {
      this.#status = ISSUE_STATUS.OPEN;
      this.#assignee = null;
    }
    this.#created_at = new Date();
  }

  static #updateUiBlock(block, issue) {
    // Remove old parts of the block
    let oldChild =
      block.querySelector('.issues__assign-to') ||
      block.querySelector('.issues__assigned');

    // New part
    let newChild = null;

    if (issue.status === ISSUE_STATUS.ASSIGNED) {
      const assignee = AssigneeList.find(issue.assignee);
      const assigneeName = assignee ? assignee.name : null;

      newChild = document.createElement('div');
      newChild.innerText = 'Assigned to ' + assigneeName;
      newChild.classList.add('issues__assigned');
    } else if (issue.status === ISSUE_STATUS.CLOSED) {
      newChild = document.createElement('div');
      newChild.innerText = issue.status;
      newChild.classList.add('issues__assigned');
    }

    // Insert new child before old child
    block.insertBefore(newChild, oldChild);

    // Remove old child
    block.removeChild(oldChild);
  }

  static #createUiBlock(issue) {
    const issueListUI = document.querySelector('.issues .issues__list');

    const block = document.createElement('li');
    block.id = issue.id;
    block.classList.add('issues__block');
    issueListUI.appendChild(block);

    const title = document.createElement('div');
    title.innerText = issue.title;
    title.classList.add('issues__title');
    block.appendChild(title);

    const deleteIssue = document.createElement('div');
    deleteIssue.innerText = 'X';
    deleteIssue.classList.add('btn', 'issues__close');
    block.appendChild(deleteIssue);
    deleteIssue.onclick = () => {
      // Update data in issue object
      issue.status = ISSUE_STATUS.CLOSED;

      // Update issueList
      const index = issueList.findIndex(i => i.id === issue.id);
      if (index !== -1) {
        issueList.splice(index, 1, issue);
      }

      Issue.#updateUiBlock(block, issue);
    };

    const description = document.createElement('p');
    description.innerText = issue.description;
    description.classList.add('issues__description');
    block.appendChild(description);

    if (issue.status === ISSUE_STATUS.ASSIGNED && issue.assignee) {
      const assignee = AssigneeList.find(issue.assignee);
      const assigneeName = assignee ? assignee.name : null;

      const assigned = document.createElement('div');
      assigned.innerText = 'Assigned to ' + assigneeName;
      assigned.classList.add('issues__assigned');
      block.appendChild(assigned);
    } else if (issue.status === ISSUE_STATUS.OPEN) {
      const assignTo = document.createElement('div');
      assignTo.classList.add('issues__assign-to');
      block.appendChild(assignTo);

      const label = document.createElement('label');
      label.innerText = 'Assign to';
      label.classList.add('issues__label');
      label.setAttribute('for', 'issues__assign-to');
      assignTo.appendChild(label);

      const input = document.createElement('input');
      input.type = 'text';
      input.id = 'issues__assign-to';
      input.name = 'issues__assign-to';
      input.classList.add('issues__input');
      input.placeholder = 'Assign to';
      input.setAttribute('list', 'issues__suggestions');
      assignTo.appendChild(input);

      const datalist = document.createElement('datalist');
      datalist.id = 'issues__suggestions';
      datalist.classList.add('assignee-name-list');
      assignTo.appendChild(datalist);

      loadAssigneeNameInDatalist();

      input.onchange = e => {
        const newAssignee = getAssigneeIdFromDatalist(e.target.value, datalist);

        if (!newAssignee) {
          alert('Invalid input. Select an assignee from dropdown.');
          return;
        }

        // Update data in issue object
        issue.assignee = newAssignee;
        issue.status = ISSUE_STATUS.ASSIGNED;

        // Update issueList
        const index = issueList.findIndex(i => i.id === issue.id);
        if (index !== -1) {
          issueList.splice(index, 1, issue);
        }

        Issue.#updateUiBlock(block, issue);
      };
    } else if (issue.status === ISSUE_STATUS.CLOSED) {
      const status = document.createElement('div');
      status.innerText = issue.status;
      status.classList.add('issues__assigned');
      block.appendChild(status);
    }

    const createdAt = document.createElement('div');
    createdAt.innerText = 'Created at ' + issue.created_at.toString();
    createdAt.classList.add('issue__time');
    block.appendChild(createdAt);
  }

  save() {
    const issue = {
      id: this.#id,
      title: this.#title,
      description: this.#description,
      status: this.#status,
      assignee: this.#assignee,
      created_at: this.#created_at
    };

    issueList.push(issue);

    Issue.#createUiBlock(issue);
  }

  static load() {
    issueList.forEach(issue => Issue.#createUiBlock(issue));
  }
}

function loadAssigneeNameInDatalist() {
  const datalists = document.querySelectorAll('.assignee-name-list');

  for (let i = 0; i < datalists.length; i++) {
    const datalist = datalists[i];

    // Clear options of datalist
    while (datalist.firstChild) {
      datalist.removeChild(datalist.firstChild);
    }

    // Add new options
    AssigneeList.list().forEach(assignee => {
      const option = document.createElement('option');
      option.value = assignee.name;
      option.setAttribute('data-value', assignee.id);
      datalist.appendChild(option);
    });
  }
}

function getAssigneeIdFromDatalist(assigneeName, datalist) {
  let datalistOptions;

  if (typeof datalist === 'string') {
    datalistOptions = document.querySelectorAll(datalist + ' option');
  } else {
    datalistOptions = datalist.querySelectorAll('option');
  }

  for (let i = 0; i < datalistOptions.length; i++) {
    const option = datalistOptions[i];

    if (option.value === assigneeName) {
      return option.getAttribute('data-value');
    }
  }

  return null;
}

const AssigneeList = new _AssigneeList();
